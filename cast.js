async function search_cast(name, page, list_status) {

    $('#cat').empty();
    $('#cat').append(`<h2><i>Let's see what we have with '${name}'</i></h2>`);

    hiding();
    $('#list').empty();

    const key = 'c35160a326e0344de330c917e176e250';
    const response = await fetch(`
        https://api.themoviedb.org/3/search/person?api_key=${key}&language=en-US&query=${name}&page=${page}
    `);

    if (response.status != 200) {
        $('#cat').append(`<h3><i>${response.status}</i></h3> `);
        list_status();
        return;
    }

    const data = await response.json();
    const list = data.results;

    if (!list.length) {
        $('#cat').append(`
            <h3><i>Oops. Something went wrong. Try again!</i></h3>
        `);
        list_status();
        return;
    }

    if (list.length == 1) {
        cast_movie(list[0].name, list[0].id, 1, showing);
        return;
    }

    for (const item of list) {

        let films = '';
        for (const x of item.known_for) {
            films += (x.original_title) ? x.original_title : x.original_name;
            films += ', ';
        }

        if (films == '')
            films = 'nothing.'
        else
            films = films.substr(0, films.length - 2) + '.';

        let img = new Image();
        img.src = `https://image.tmdb.org/t/p/w185_and_h278_bestv2${item.profile_path}`;

        $('#list').append(`
            <div class="card sd border-dark bg-tran-1 cur-select hl w-50" onclick="cast_movie('${item.name}', ${item.id}, 1, showing)">
                <div class="row">
                    <div class="col-2">
                        <img class="card-img" src=${img.src} alt="Poster" onerror="this.src='img/error.jpg'">
                    </div>
                    <div class="col-10">
                        <div class="card-body">
                            <a class="a-img" href="#">
                                <h4 class="card-title">${item.name}</h4>
                            </a>
                            <p class="card-text text-truncate"><strong>Known for: </strong>${films}</p>
                        </div>
                    </div>
                </div>
            </div>
        `);
    }

    if (data.total_pages > 1) {
        $('#list').append(`
            <div class="col-12 mt-5">
                <nav aria-label="Page navigation">
                    <ul class="pagination justify-content-center">
                        <li id="prev" class="page-item" data-toggle="tooltip" title="Page 1">
                            <a class="page-link" href="#" onclick="search_cast('${name}', 1, showing)">
                                <span aria-hidden="true">&laquo;</span>
                            </a>
                        </li>
                    </ul>
                </nav>
            </div>
        `);

        let first = (page < 6) ? 1 : page - 4;
        let last = (page < 6) ? 10 : page + 5;

        for (i = first; i <= data.total_pages && i < last; ++i) {
            $('[class="pagination justify-content-center"]').append(`
                <li id="pg${i}" class="page-item">
                    <a class="page-link" href="#" onclick="search_cast('${name}', ${i}, showing)">${i}</a>
                </li>
            `);
        }

        $('[class="pagination justify-content-center"]').append(`
            <li id="next" class="page-item" data-toggle="tooltip" title="Page ${data.total_pages}">
                <a class="page-link" href="#" onclick="search_cast('${name}', ${data.total_pages}, showing)">
                    <span aria-hidden="true">&raquo;</span>
                </a>
            </li>
        `);

        $(`#pg${page}`).addClass('active');

        if (page == 1)
            $('#prev').addClass('disabled');
        if (page == data.total_pages)
            $('#next').addClass('disabled');
    }

    list_status();
}

async function cast_movie(input, person_id, page, cb) {

    $('#cat').empty();
    $('#cat').append(`
        <h2><i>'${input}' movies:</i></h2>
    `);

    hiding();
    $('#list').empty();

    const key = 'c35160a326e0344de330c917e176e250';
    const response = await fetch(`
        https://api.themoviedb.org/3/person/${person_id}/movie_credits?api_key=${key}&language=en-US
    `);

    if (response.status != 200) {
        $('#cat').append(`
            <h3><i>${response.status}</i></h3>
        `);
        list_status();
        return;
    }

    const data = await response.json();
    const list = data.cast;

    if (!list.length) {
        $('#cat').append(`
            <h3><i>Oops. Something went wrong. Try again!</i></h3>
        `);
        list_status();
        return;
    }

    if (list.length == '1') {
        movie_info(list[0].id, showing);
        return;
    }

    let l = (page - 1) * 20;
    console.log(l);
    console.log(list.length);

    for (k = l; k < list.length && k < l + 20; ++k) {

        if (k % 5 == 0) {
            $('#list').append(`
            <div id="row${parseInt(k / 5)}" class="row justify-content-center w-100"></div>
        `);
        }

        let date = 'Unknown';
        if (list[k].release_date) {
            date = new Date(list[k].release_date).toLocaleString('en-GB', {
                year: 'numeric',
                month: 'long',
                day: 'numeric'
            });
        }

        let rating = '';
        let n = Math.round(list[k].vote_average / 2)
        for (i = 0; i < n; ++i)
            rating += '&#9733';
        for (i = n; i < 5; ++i)
            rating += '&#9734';
        rating += '(' + list[k].vote_count + ')';

        let img = new Image();
        img.src = `https://image.tmdb.org/t/p/w300_and_h450_bestv2${list[k].poster_path}`;

        $(`#row${parseInt(k / 5)}`).append(`
        <div class="col-2 mb-4">
            <div class="card sd border-dark cur-select hl h-100" onclick="movie_info(${list[k].id}, showing)">
                <img class="card-img" src=${img.src} alt="Poster" onerror="this.src='img/error.jpg'">
                
                <div class="card-img-overlay d-flex flex-column justify-content-end">
                    <a class="a-img text-center" href="#">
                        <h4 class="card-title mb-0">${list[k].title}</h4>
                        <p class="card-text text-light mb-1">${date}</p>
                        <p class="card-text text-warning">${rating}</p>
                    </a>
                </div>
            </div>
        </div>
    `);
    }

    if (list.length > 20) {
        $('#list').append(`
            <div class="col-12">
                <nav aria-label="Page navigation">
                    <ul class="pagination justify-content-center">
                        <li id="prev" class="page-item" data-toggle="tooltip" title="Page 1">
                            <a class="page-link" href="#" onclick="cast_movie('${input}', ${person_id}, 1, showing)">
                                <span aria-hidden="true">&laquo;</span>
                            </a>
                        </li>
                    </ul>
                </nav>
            </div>
        `);

        let first = (page < 6) ? 1 : page - 4;
        let last = (page < 6) ? 10 : page + 5;
        let l = parseInt(list.length / 20) + 1;

        for (i = first; i <= l && i < last; ++i) {
            $('[class="pagination justify-content-center"]').append(`
                <li id="pg${i}" class="page-item">
                    <a class="page-link" href="#" onclick="cast_movie('${input}', ${person_id}, ${i}, showing)">${i}</a>
                </li>
            `);
        }

        $('[class="pagination justify-content-center"]').append(`
            <li id="next" class="page-item" data-toggle="tooltip" title="Page ${l}">
                <a class="page-link" href="#" onclick="cast_movie('${input}', ${person_id}, ${l}, showing)">
                    <span aria-hidden="true">&raquo;</span>
                </a>
            </li>
        `);

        $(`#pg${page}`).addClass('active');

        if (page == 1)
            $('#prev').addClass('disabled');
        if (page == l)
            $('#next').addClass('disabled');
    }

    list_status();
}

async function cast_info(person_id, list_status) {

    $('#cat').empty();
    hiding();
    $('#list').empty();

    const key = 'c35160a326e0344de330c917e176e250';
    const response = await fetch(`
        https://api.themoviedb.org/3/person/${person_id}?api_key=${key}&language=en-US&append_to_response=movie_credits
    `);

    if (response.status != 200) {
        $('#cat').append(`
            <h3><i>${response.status}</i></h3>
        `);
        list_status();
        return;
    }

    const item = await response.json();
    const credits = item.movie_credits.cast;

    document.title = item.name;

    let date = 'Unknown';
    if (item.birthday) {
        date = new Date(item.birthday).toLocaleString('en-GB', {
            year: 'numeric',
            month: 'long',
            day: 'numeric'
        });
    }

    let img = new Image();
    img.src = `https://image.tmdb.org/t/p/w600_and_h900_bestv2${item.profile_path}`;

    $('#list').append(`
        <div class="card sd border-dark bg-tran-1 mb-3 w-100">
            <div class="row no-gutters">
                <div class="col-4">
                    <img class="card-img" src=${img.src} alt="Poster" onerror="this.src='img/error.jpg'">
                </div>
                <div class="col-8">
                    <div class="card-body">
                        <h3 class="card-title text-success mb-0">${item.name}</h3>
                        <p class="card-text text-secondary ">${date}</p>
                        <p class="card-text"><h5>Biography: </h5>${item.biography}</p>
                        <p class="card-text"><h5>Known for: </h5></p>
                        <div id="cc" class="carousel card-carousel slide mt-5" data-ride="carousel" data-interval="false">
                            <div class="carousel-inner"></div>
                            <a class="carousel-control-prev" href="#cc" role="button" data-slide="prev">
                                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                <span class="sr-only">Previous</span>
                            </a>
                            <a class="carousel-control-next" href="#cc" role="button" data-slide="next">
                                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                <span class="sr-only">Next</span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    `);

    if (!credits.length) {
        $('#cc').empty();
        $('#cc').append('<p class="card-text ml-5">Unknown</p>');
    } else {
        var indicator = Math.ceil(credits.length / 2);
        for (i = 0; i < indicator; ++i) {
            $('[class="carousel-inner"]').append(`
                <div id="cc${i}" class="carousel-item">
                    <div class="row"></div>
                </div>
            `);
            let n = Math.min(i * 2 + 2, credits.length);
            for (j = i * 2; j < n; ++j) {

                let date = 'Unknown';
                if (credits[j].release_date) {
                    date = new Date(credits[j].release_date).toLocaleString('en-GB', {
                        year: 'numeric',
                        month: 'long',
                        day: 'numeric'
                    });
                }

                var rating = '';
                for (k = 0; k < parseInt(credits[j].vote_average / 2); ++k)
                    rating += '&#9733';
                for (k = parseInt(credits[j].vote_average / 2); k < 5; ++k)
                    rating += '&#9734';
                rating += '(' + credits[j].vote_count + ')';

                let as = (credits[j].character) ? credits[j].character : credits[j].job;
                if (!as)
                    as = 'Unknown';

                let img = new Image();
                img.src = `https://image.tmdb.org/t/p/w185_and_h278_bestv2${credits[j].poster_path}`;

                $(`#cc${i}`).children().append(`
                    <div class="card sd border-dark bg-tran-1 w-100 ml-5 mr-5">
                        <div class="row justify-content-center">
                            <div class="col-2 cur-select" onclick="movie_info(${credits[j].id}, showing)">
                                <img class="card-img" src=${img.src} alt="Poster" onerror="this.src='img/error.jpg'">
                            </div>
                            <div class="col-4">
                                <div class="card-body">
                                    <a class="a-img" href="#" onclick="movie_info(${credits[j].id}, showing)">
                                        <h5 class="card-title mb-0">${credits[j].title}</h5>
                                    </a>
                                    <p class="card-text text-secondary mb-1">${date}</p>
                                    <p class="card-text text-danger">${rating}</p>
                                    <p class="card-text"><strong>As: </strong>${as}</p>
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="card-body">
                                    <h5 class="card-text">Overview: </h5>
                                    <p class="card-text text-truncate">${credits[j].overview}</p>
                                </div>
                            </div>
                        </div>
                    </div>
                `);
            }
        }

        $('[data-slide-to="0"]').addClass('active');
        $('#cc0').addClass('active');
    }

    list_status();
}