function hiding() {
    $('#list').hide();
}

function showing() {
    $('#list').show();
}

async function get_list(type, page, list_status) {

    $('#cat').empty();
   

    if (type == 'popular') {
        $('#cat').children().eq(0).addClass('active');
    }

    hiding();
    $('#list').empty();

    const key = 'c35160a326e0344de330c917e176e250';
    const response = await fetch(` https://api.themoviedb.org/3/movie/${type}?api_key=${key}&page=${page} `);

    if (response.status != 200) {
        $('#cat').append(` <h3><i>${response.status}</i></h3> `);
        list_status();
        return;
    }

    const data = await response.json();
    const list = data.results;

    if (!list.length) {
        $('#cat').append(` <h3><i> Oops. Something went wrong. Try again! </i></h3> `);
        list_status();
        return;
    }

    let k = 0;
    for (const item of list) {

        if (k % 5 == 0) {
            $('#list').append(`
                <div id="row${parseInt(k / 5)}" class="row justify-content-center w-100"></div>
            `);
        }

        let date = 'Unknown';
        if (item.release_date) {
            date = new Date(item.release_date).toLocaleString('en-GB', {
                year: 'numeric',
                month: 'long',
                day: 'numeric'
            });
        }

        let rating = '';
        let n = Math.round(item.vote_average / 2)
        for (i = 0; i < n; ++i)
            rating += '&#9733';
        for (i = n; i < 5; ++i)
            rating += '&#9734';
        rating += '(' + item.vote_count + ')';

        let img = new Image();
        img.src = `https://image.tmdb.org/t/p/w300_and_h450_bestv2${item.poster_path}`;

        $(`#row${parseInt(k / 5)}`).append(`
            <div class="col-2 mb-4">
                <div class="card sd border-dark cur-select hl h-100" onclick="movie_info(${item.id}, showing)">
                    <img class="card-img" src=${img.src} alt="Poster" onerror="this.src='img/No_picture_available.png'">
                    
                    <div class="card-img-overlay d-flex flex-column justify-content-end">
                        <a class="a-img text-center" href="#">
                            <h5 class="card-title mb-0">${item.title}</h5>
                            <p class="card-text text-warning">${rating}</p>
                            <p class="card-text text-light mb-1">${date}</p>
                        </a>
                    </div>
                </div>
            </div>
        `);
        ++k;
    }

    if (data.total_pages > 1) {
        $('#list').append(`
            <div class="col-12">
                <nav aria-label="Page navigation">
                    <ul class="pagination justify-content-center">
                        <li id="prev" class="page-item" data-toggle="tooltip" title="Page 1">
                            <a class="page-link" href="#" onclick="get_list('${type}', 1, showing)">
                                <span aria-hidden="true">&laquo;</span>
                            </a>
                        </li>
                    </ul>
                </nav>
            </div>
        `);

        let first = (page < 6) ? 1 : page - 4;
        let last = (page < 6) ? 10 : page + 5;

        for (i = first; i <= data.total_pages && i < last; ++i) {
            $('[class="pagination justify-content-center"]').append(`
                <li id="pg${i}" class="page-item">
                    <a class="page-link" href="#" onclick="get_list('${type}', ${i}, showing)">${i}</a>
                </li>
            `);
        }

        $('[class="pagination justify-content-center"]').append(`
            <li id="next" class="page-item" data-toggle="tooltip" title="Page ${data.total_pages}">
                <a class="page-link" href="#" onclick="get_list('${type}', ${data.total_pages}, showing)">
                    <span aria-hidden="true">&raquo;</span>
                </a>
            </li>
        `);

        $(`#pg${page}`).addClass('active');

        if (page == 1)
            $('#prev').addClass('disabled');
        if (page == data.total_pages)
            $('#next').addClass('disabled');
    }

    list_status();
}

function search_input() {

    if (!$('#search').val())
        return;

    if ($('select').val() == 'movie') {
         search_movie($('#search').val(), 1, showing);
    } else {
         search_cast($('#search').val(), 1, showing);
    }
}



